require 'selenium-webdriver'
require 'page-object'
require 'require_all'
require 'yaml'
require 'cucumber'
require 'rspec'
require_all './lib'
$testData = YAML.load_file('config/testData.YML')