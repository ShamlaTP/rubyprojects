Given(/^I enter url$/) do
  @search=SearchText.new($browser)
  @search.visit_url
end

When(/^I enter course subject as "([^"]*)" in search field$/) do |text|
  @text = text
  @search.searchField(text)
end

Then(/^I should be able to capture the number of results found$/) do
#  @search.searchResult
  numberOfCourses=$browser.find_element(:css, 'span.udlite-heading-md.filter-panel--item-count--2JGx3').text
  puts "Number of Results : #{numberOfCourses}"
end

And(/^I should see valid text  in course name field$/) do
  val=$browser.find_element(:xpath, '//div[@class="udlite-heading-sm udlite-focus-visible-target course-card--course-title--2f7tE"]').text
  expect(val).to include @text
  puts "Assertion Success : #{@text} is present in #{val}"
end