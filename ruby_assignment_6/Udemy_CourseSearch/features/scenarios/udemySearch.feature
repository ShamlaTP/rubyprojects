Feature: Udemy Course Search
  Scenario: Udemy Course Search validations
    Given I enter url
    When I enter course subject as "Ruby" in search field
    Then I should be able to capture the number of results found
    And I should see valid text  in course name field