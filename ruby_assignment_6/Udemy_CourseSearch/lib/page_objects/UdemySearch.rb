class SearchText
  include PageObject

  text_field  :searchField, id:'search-field-home'
  image :searchResult, css:'span.udlite-heading-md.filter-panel--item-count--2JGx3'
  image :searchResultCourse, xpath:'//div[@class="udlite-heading-sm udlite-focus-visible-target course-card--course-title--2f7tE"]'
  def visit_url
    $browser.navigate.to($testData['url'])

  end
  def searchField(text)
    searchField_element.send_keys(text,:enter)
    sleep 4
  end

  def searchResult

    @result = searchResult_element.text
    puts "Number of Results : #{@result}"
  end

  def searchResultCourse
    @courseName=searchResultCourse_element.text
    puts @courseName
  end

end
